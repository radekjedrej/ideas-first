import { vh, handleWindow } from './modules/_helpers'
import LazyLoad from 'vanilla-lazyload'
import ScrollDown from './modules/_scroll-down'
import SwiperCarousel from './modules/_swiper'
import MobileMenu from './modules/_mobile-menu'
import modal from 'jquery-modal'
import SmoothScroll from 'smooth-scroll'
import { hashCheck } from './modules/_hash-check'
import MobileScroll from './modules/_mobile-scroll'

window.addEventListener('DOMContentLoaded', () => {
    const fullBanner = document.querySelector(".full-banner__wrapper");
    let current = window.location.pathname;

    // Variables
    hashCheck();
    
    vh();

    // Object Instance
    const myLazyLoad = new LazyLoad();
    
    new MobileMenu;

    new SmoothScroll('a[href*="#"]', {
        header: '.l-navbar__offset',
        speed: 300
    });

    if(fullBanner) {
        new ScrollDown();
    }

    new SwiperCarousel();
    new handleWindow();

    if (current == '/') {
        new MobileScroll();
    }
})
