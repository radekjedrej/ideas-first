import SmoothScroll from 'smooth-scroll'

export const hashCheck = () => {
    if(window.location.hash) {
        let hashLink = window.location.hash;
        const element = document.querySelector(hashLink);
        
        if(element) {
            setTimeout(function() {
                if (hashLink.length) {
                    let scroll = new SmoothScroll();
                    var options = { speed: 1, speedAsDuration: true, header: '.l-navbar__offset' };
                    scroll.animateScroll(element, false, options)
                }
            }, 50)
        }
    }
}