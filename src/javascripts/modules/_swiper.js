import Swiper, { Navigation, Autoplay, Pagination, EffectFade } from 'swiper'

class SwiperCarousel {
    constructor() {
        Swiper.use([Navigation, Autoplay, Pagination, EffectFade]);
        
        this.companies
        this.resources
        this.fullBanner
        this.init()
    }

    init() {
        this.companiesLogos()
        this.resourcesItems()
        this.fullBannerCarousel()
    }
    
    companiesLogos() {
        this.companies = new Swiper(".double-carousel__container", {
            slidesPerView: 2,
            freeMode: true,
            lazy: true,
            autoplay: {
                delay: 6000,
            },
            slidesPerColumn: 2,
            slidesPerColumnFill: 'row',
            spaceBetween: 30,
            navigation: {
                nextEl: ".double-carousel__right",
                prevEl: ".double-carousel__left",
            },
            breakpoints: {
                768: {
                slidesPerView: 3,
                resistanceRatio: 0,
                slidesPerColumn: 2,
                spaceBetween: 70,
                },

                1280: {
                slidesPerView: 4,
                resistanceRatio: 0,
                slidesPerColumn: 2,
                spaceBetween: 40
                }
            }
        });
    } 

    resourcesItems() {
        this.resources = new Swiper(".carousel-resources__container", {
            slidesPerView: "auto",
            lazy: true,
            loop: true,
            autoplay: {
                delay: 6000,
            },
            spaceBetween: 16,
            navigation: {
                nextEl: ".carousel-resources__right",
                prevEl: ".carousel-resources__left",
            },
            breakpoints: {
                480: {
                slidesPerView: 2,
                resistanceRatio: 0,
                spaceBetween: 32,
                },

                1024: {
                slidesPerView: 3,
                resistanceRatio: 0,
                spaceBetween: 65
                }
            }
        });
    }

    fullBannerCarousel() {
        this.fullBanner = new Swiper(".full-banner__image", {
            slidesPerView: 1,
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            speed: 2000,
            lazy: true,
            loop: true,
            autoplay: {
                delay: 6000,
            },
        });
    }
}

export default SwiperCarousel