import $ from 'jquery'

class MobileMenu {
    constructor() {
        this.html = $("html");
        this.navbar = $(".l-navbar");
        this.items = $(".m-menu__item");
        this.dropdown = $(".m-menu__dropdown");
        this.burgerContainer = $(".l-navbar__menu");
        this.menu = $(".m-menu"); 
        this.events();
    }

    events() {
        this.burgerContainer.children(".l-navbar__burger").on("click", this.openMenu.bind(this))

        this.submenu()
    }

    openMenu() {
        this.items.removeClass("active");
        this.dropdown.slideUp();
        this.burgerContainer.toggleClass("toggle")
        this.html.toggleClass("no-scroll");
        this.navbar.toggleClass("active")
        this.menu.slideToggle();
        this.menu.toggleClass("open");
    }

    submenu() {
        const dropdownParent = this.dropdown.parent()
        dropdownParent.addClass("navbar__has-child")
        dropdownParent.on('click', function ToggleSubMenuOpen(e){
            if(!$(e.target).is(".m-menu__dropdown__link")){
                e.preventDefault();
                $(this).toggleClass("active")
                $(this).find(".m-menu__dropdown").slideToggle(400, "swing");
            } else {
                return;
            }
        })
    }
}

export default MobileMenu