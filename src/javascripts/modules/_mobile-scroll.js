import $ from 'jquery'

class MobileScroll {
    constructor() {
        this.html = $("html");
        this.navbar = $(".l-navbar");
        this.items = $(".m-menu__item");
        this.dropdown = $(".m-menu__dropdown");
        this.menu = $(".m-menu"); 
        this.link = $(".m-menu__link");
        this.events();
    }

    events() {
        this.link.on("click", this.handleClick.bind(this))
    }

    handleClick(e) {

        if((e.currentTarget).hasAttribute('data-scroll')) {
            this.items.removeClass("active");
            this.dropdown.slideUp();
            this.html.toggleClass("no-scroll");
            this.navbar.toggleClass("active")
            this.menu.slideToggle();
            this.menu.toggleClass("open");
        }
    }
}

export default MobileScroll