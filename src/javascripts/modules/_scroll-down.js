import $ from 'jquery'
import SmoothScroll from 'smooth-scroll'

class ScrollDown {
    constructor() {
        this.body = $("html, body")
        this.bannerHeight = $(".full-banner").height();
        this.button = $(".full-banner__circle")
        this.scroll
        this.events();
        this.scroll = new SmoothScroll();
    }

    events() {
        this.button.on("click", this.handleScroll.bind(this))
    }

    handleScroll() {
        var options = { speed: 800 };
        this.scroll.animateScroll(this.bannerHeight, false, options);
    }
}

export default ScrollDown